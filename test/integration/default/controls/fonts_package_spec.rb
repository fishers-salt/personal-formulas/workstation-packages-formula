# frozen_string_literal: true

fonts = %w[
  adobe-source-code-pro-fonts
  gnu-free-fonts
  noto-fonts-cjk
  noto-fonts-emoji
  ttf-fira-code
]

fonts.each do |pkg|
  control "workstation-packages-fonts-#{pkg}-install-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end

control 'workstation-packages-fonts-install-custom-font-dir-managed' do
  title 'should exist'

  describe directory('/usr/local/share/fonts') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0755' }
  end
end

control 'workstation-packages-fonts-install-custom-font-powerlevel-dir-managed' do
  title 'should exist'

  describe directory('/usr/local/share/fonts/Powerlevel10k') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0755' }
  end
end

control 'workstation-packages-fonts-install-powerlevel-regular-font-managed' do
  title 'should exist'

  describe file('/usr/local/share/fonts/Powerlevel10k/MesloLGS NF Regular.ttf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
end

control 'workstation-packages-fonts-install-powerlevel-bold-italic-font-managed' do
  title 'should exist'

  describe file('/usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold Italic.ttf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
end

control 'workstation-packages-fonts-install-powerlevel-bold-font-managed' do
  title 'should exist'

  describe file('/usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold.ttf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
end

control 'workstation-packages-fonts-install-powerlevel-italic-font-managed' do
  title 'should exist'

  describe file('/usr/local/share/fonts/Powerlevel10k/MesloLGS NF Italic.ttf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
end
