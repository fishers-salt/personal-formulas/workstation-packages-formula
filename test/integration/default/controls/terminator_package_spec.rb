# frozen_string_literal: true

control 'workstation-packages-terminator-install-installed' do
  title 'should be installed'

  describe package('terminator') do
    it { should be_installed }
  end
end
