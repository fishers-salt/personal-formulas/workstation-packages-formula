# frozen_string_literal: true

control 'workstation-packages-ctags-install-installed' do
  title 'should be installed'

  describe package('ctags') do
    it { should be_installed }
  end
end
