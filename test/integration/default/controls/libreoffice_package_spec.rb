# frozen_string_literal: true

libreoffice = %w[libreoffice-fresh libreoffice-fresh-en-gb jre8-openjdk]

libreoffice.each do |pkg|
  control "workstation-packages-libreoffice-#{pkg}-install-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
