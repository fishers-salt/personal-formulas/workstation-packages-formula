# frozen_string_literal: true

control 'workstation-packages-inkscape-install-installed' do
  title 'should be installed'

  describe package('inkscape') do
    it { should be_installed }
  end
end
