# frozen_string_literal: true

control 'workstation-packages-yt-dlp-install-installed' do
  title 'should be installed'

  describe package('yt-dlp') do
    it { should be_installed }
  end
end
