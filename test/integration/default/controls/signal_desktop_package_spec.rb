# frozen_string_literal: true

control 'workstation-packages-signal-desktop-install-installed' do
  title 'should be installed'

  describe package('signal-desktop') do
    it { should be_installed }
  end
end
