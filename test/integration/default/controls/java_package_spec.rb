# frozen_string_literal: true

control 'workstation-packages-java-install-installed' do
  title 'should be installed'

  describe package('jre-openjdk') do
    it { should be_installed }
  end
end
