# frozen_string_literal: true

control 'workstation-packages-terraform-install-installed' do
  title 'should be installed'

  describe package('terraform') do
    it { should be_installed }
  end
end
