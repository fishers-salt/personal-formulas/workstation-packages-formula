# frozen_string_literal: true

control 'workstation-packages-vagrant-install-installed' do
  title 'should be installed'

  describe package('vagrant') do
    it { should be_installed }
  end
end
