# frozen_string_literal: true

control 'workstation-packages-pre-commit-install-installed' do
  title 'should be installed'

  describe package('python-pre-commit') do
    it { should be_installed }
  end
end
