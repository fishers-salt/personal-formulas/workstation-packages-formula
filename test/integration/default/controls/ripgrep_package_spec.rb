# frozen_string_literal: true

control 'workstation-packages-ripgrep-install-installed' do
  title 'should be installed'

  describe package('ripgrep') do
    it { should be_installed }
  end
end
