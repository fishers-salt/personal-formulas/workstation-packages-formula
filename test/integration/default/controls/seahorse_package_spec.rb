# frozen_string_literal: true

control 'workstation-packages-seahorse-install-installed' do
  title 'should be installed'

  describe package('seahorse') do
    it { should be_installed }
  end
end
