# frozen_string_literal: true

control 'workstation-packages-evince-install-installed' do
  title 'should be installed'

  describe package('evince') do
    it { should be_installed }
  end
end
