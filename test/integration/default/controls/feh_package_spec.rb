# frozen_string_literal: true

control 'workstation-packages-feh-install-installed' do
  title 'should be installed'

  describe package('feh') do
    it { should be_installed }
  end
end
