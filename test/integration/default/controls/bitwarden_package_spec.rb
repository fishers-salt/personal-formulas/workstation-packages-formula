# frozen_string_literal: true

bitwarden = %w[bitwarden bitwarden-cli]

bitwarden.each do |bw|
  control "workstation-packages-bitwarden-#{bw}-install-installed" do
    title 'should be installed'

    describe package(bw) do
      it { should be_installed }
    end
  end
end
