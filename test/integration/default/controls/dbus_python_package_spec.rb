# frozen_string_literal: true

control 'workstation-packages-dbus-python-install-installed' do
  title 'should be installed'

  describe package('dbus-python') do
    it { should be_installed }
  end
end
