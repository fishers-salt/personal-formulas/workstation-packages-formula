# frozen_string_literal: true

control 'workstation-packages-kdeconnect-install-installed' do
  title 'should be installed'

  describe package('kdeconnect') do
    it { should be_installed }
  end
end
