# frozen_string_literal: true

control 'workstation-packages-brightnessctl-install-installed' do
  title 'should be installed'

  describe package('brightnessctl') do
    it { should be_installed }
  end
end
