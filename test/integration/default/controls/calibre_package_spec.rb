# frozen_string_literal: true

control 'workstation-packages-calibre-install-installed' do
  title 'should be installed'

  describe package('calibre') do
    it { should be_installed }
  end
end
