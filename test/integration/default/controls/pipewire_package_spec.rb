# frozen_string_literal: true

pipewire = %w[
  pipewire
  pipewire-alsa
  pipewire-audio
  pipewire-jack
  pipewire-pulse
  wireplumber
]

pipewire.each do |pkg|
  control "workstation-packages-pipewire-#{pkg}-install-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
