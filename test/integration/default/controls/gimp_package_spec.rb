# frozen_string_literal: true

control 'workstation-packages-gimp-install-installed' do
  title 'should be installed'

  describe package('gimp') do
    it { should be_installed }
  end
end
