# frozen_string_literal: true

control 'workstation-packages-neovim-qt-install-installed' do
  title 'should be installed'

  describe package('neovim-qt') do
    it { should be_installed }
  end
end
