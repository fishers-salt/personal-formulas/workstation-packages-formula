# frozen_string_literal: true

deps = %w[libcamera libcamera-ipa]

deps.each do |pkg|
  control "workstation-packages-gnome-#{pkg}-deps-install-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end

gnome = %w[gedit gnome-boxes gnome-terminal gnome-session nautilus]

gnome.each do |pkg|
  control "workstation-packages-gnome-#{pkg}-install-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
