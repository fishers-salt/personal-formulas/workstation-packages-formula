# frozen_string_literal: true

control 'workstation-packages-telegram-desktop-install-installed' do
  title 'should be installed'

  describe package('telegram-desktop') do
    it { should be_installed }
  end
end
