# frozen_string_literal: true

zathura = %w[zathura zathura-cb zathura-djvu zathura-pdf-mupdf zathura-ps]

zathura.each do |pkg|
  control "workstation-packages-zathura-#{pkg}-install-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
