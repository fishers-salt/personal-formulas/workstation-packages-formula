# frozen_string_literal: true

control 'workstation-packages-seahorse-clean-removed' do
  title 'should not be installed'

  describe package('seahorse') do
    it { should_not be_installed }
  end
end
