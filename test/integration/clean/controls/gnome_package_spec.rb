# frozen_string_literal: true

gnome = %w[gedit gnome-boxes gnome-terminal gnome-session nautilus]

gnome.each do |pkg|
  control "workstation-packages-gnome-#{pkg}-clean-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end

deps = %w[libcamera libcamera-ipa]

deps.each do |pkg|
  control "workstation-packages-gnome-deps-#{pkg}-clean-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
