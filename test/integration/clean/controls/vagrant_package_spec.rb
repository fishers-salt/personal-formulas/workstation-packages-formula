# frozen_string_literal: true

control 'workstation-packages-vagrant-clean-removed' do
  title 'should not be installed'

  describe package('vagrant') do
    it { should_not be_installed }
  end
end
