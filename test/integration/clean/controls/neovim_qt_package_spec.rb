# frozen_string_literal: true

control 'workstation-packages-neovim-qt-clean-removed' do
  title 'should not be installed'

  describe package('neovim-qt') do
    it { should_not be_installed }
  end
end
