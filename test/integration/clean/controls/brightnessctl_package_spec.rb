# frozen_string_literal: true

control 'workstation-packages-brightnessctl-clean-removed' do
  title 'should not be installed'

  describe package('brightnessctl') do
    it { should_not be_installed }
  end
end
