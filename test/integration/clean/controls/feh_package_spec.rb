# frozen_string_literal: true

control 'workstation-packages-feh-clean-removed' do
  title 'should not be installed'

  describe package('feh') do
    it { should_not be_installed }
  end
end
