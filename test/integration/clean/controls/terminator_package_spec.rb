# frozen_string_literal: true

control 'workstation-packages-terminator-clean-removed' do
  title 'should not be installed'

  describe package('terminator') do
    it { should_not be_installed }
  end
end
