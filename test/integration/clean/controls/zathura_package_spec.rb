# frozen_string_literal: true

zathura = %w[zathura zathura-cb zathura-djvu zathura-pdf-mupdf zathura-ps]

zathura.each do |pkg|
  control "workstation-packages-zathura-#{pkg}-clean-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
