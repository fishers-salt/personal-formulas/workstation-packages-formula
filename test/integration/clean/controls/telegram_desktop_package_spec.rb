# frozen_string_literal: true

control 'workstation-packages-telegram-desktop-clean-removed' do
  title 'should not be installed'

  describe package('telegram-desktop') do
    it { should_not be_installed }
  end
end
