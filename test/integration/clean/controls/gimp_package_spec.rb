# frozen_string_literal: true

control 'workstation-packages-gimp-clean-removed' do
  title 'should not be installed'

  describe package('gimp') do
    it { should_not be_installed }
  end
end
