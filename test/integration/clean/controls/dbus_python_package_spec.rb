# frozen_string_literal: true

control 'workstation-packages-dbus-python-clean-removed' do
  title 'should not be installed'

  describe package('dbus-python') do
    it { should_not be_installed }
  end
end
