# frozen_string_literal: true

control 'workstation-packages-signal-desktop-clean-removed' do
  title 'should not be installed'

  describe package('signal-desktop') do
    it { should_not be_installed }
  end
end
