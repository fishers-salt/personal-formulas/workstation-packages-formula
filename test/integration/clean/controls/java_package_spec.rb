# frozen_string_literal: true

control 'workstation-packages-java-clean-removed' do
  title 'should not be installed'

  describe package('jre-openjdk') do
    it { should_not be_installed }
  end
end
