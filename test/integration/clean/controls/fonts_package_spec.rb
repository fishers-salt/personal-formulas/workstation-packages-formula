# frozen_string_literal: true

fonts = %w[
  adobe-source-code-pro-fonts
  gnu-free-fonts
  noto-fonts-cjk
  noto-fonts-emoji
  ttf-fira-code
]

fonts.each do |pkg|
  control "workstation-packages-fonts-#{pkg}-clean-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end

control 'workstation-packages-fonts-install-custom-font-powerlevel-dir-managed' do
  title 'should not exist'

  describe directory('/usr/local/share/fonts/Powerlevel10k') do
    it { should_not exist }
  end
end
