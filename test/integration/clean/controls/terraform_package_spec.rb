# frozen_string_literal: true

control 'workstation-packages-terraform-clean-removed' do
  title 'should not be installed'

  describe package('terraform') do
    it { should_not be_installed }
  end
end
