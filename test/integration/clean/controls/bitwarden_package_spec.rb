# frozen_string_literal: true

bitwarden = %w[bitwarden bitwarden-cli]

bitwarden.each do |bw|
  control "workstation-packages-bitwarden-#{bw}-clean-removed" do
    title 'should not be installed'

    describe package(bw) do
      it { should_not be_installed }
    end
  end
end
