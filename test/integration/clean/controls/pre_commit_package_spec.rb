# frozen_string_literal: true

control 'workstation-packages-pre-commit-clean-removed' do
  title 'should not be installed'

  describe package('python-pre-commit') do
    it { should_not be_installed }
  end
end
