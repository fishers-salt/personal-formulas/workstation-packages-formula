# frozen_string_literal: true

control 'workstation-packages-calibre-clean-removed' do
  title 'should not be installed'

  describe package('calibre') do
    it { should_not be_installed }
  end
end
