# frozen_string_literal: true

control 'workstation-packages-inkscape-clean-removed' do
  title 'should not be installed'

  describe package('inkscape') do
    it { should_not be_installed }
  end
end
