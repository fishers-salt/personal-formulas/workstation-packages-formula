# frozen_string_literal: true

control 'workstation-packages-ripgrep-clean-removed' do
  title 'should not be installed'

  describe package('ripgrep') do
    it { should_not be_installed }
  end
end
