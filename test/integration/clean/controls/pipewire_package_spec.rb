# frozen_string_literal: true

pipewire = %w[
  pipewire
  pipewire-alsa
  pipewire-audio
  pipewire-jack
  pipewire-pulse
  wireplumber
]

pipewire.each do |pkg|
  control "workstation-packages-pipewire-#{pkg}-clean-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
