# frozen_string_literal: true

control 'workstation-packages-ctags-clean-removed' do
  title 'should not be installed'

  describe package('ctags') do
    it { should_not be_installed }
  end
end
