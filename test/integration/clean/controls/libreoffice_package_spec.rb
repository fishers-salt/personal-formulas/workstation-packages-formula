# frozen_string_literal: true

libreoffice = %w[libreoffice-fresh libreoffice-fresh-en-gb jre8-openjdk]

libreoffice.each do |pkg|
  control "workstation-packages-libreoffice-#{pkg}-clean-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
