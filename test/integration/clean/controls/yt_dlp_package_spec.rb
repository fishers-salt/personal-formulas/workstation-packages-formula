# frozen_string_literal: true

control 'workstation-packages-yt-dlp-clean-removed' do
  title 'should not be installed'

  describe package('yt-dlp') do
    it { should_not be_installed }
  end
end
