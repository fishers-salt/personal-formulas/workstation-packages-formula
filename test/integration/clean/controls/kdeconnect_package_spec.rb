# frozen_string_literal: true

control 'workstation-packages-kdeconnect-clean-removed' do
  title 'should not be installed'

  describe package('kdeconnect') do
    it { should_not be_installed }
  end
end
