# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .bitwarden.clean
  - .brightnessctl.clean
  - .calibre.clean
  - .ctags.clean
  - .dbus_python.clean
  - .evince.clean
  - .feh.clean
  - .fonts.clean
  - .gimp.clean
  - .gnome.clean
  - .inkscape.clean
  - .java.clean
  - .kdeconnect.clean
  - .libreoffice.clean
  - .neovim_qt.clean
  - .pipewire.clean
  - .pre_commit.clean
  - .ripgrep.clean
  - .seahorse.clean
  - .signal_desktop.clean
  - .telegram_desktop.clean
  - .terminator.clean
  - .terraform.clean
  - .vagrant.clean
  - .yt_dlp.clean
  - .zathura.clean
