# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .bitwarden
  - .brightnessctl
  - .calibre
  - .ctags
  - .dbus_python
  - .evince
  - .feh
  - .fonts
  - .gimp
  - .gnome
  - .inkscape
  - .java
  - .kdeconnect
  - .libreoffice
  - .neovim_qt
  - .pipewire
  - .pre_commit
  - .ripgrep
  - .seahorse
  - .signal_desktop
  - .telegram_desktop
  - .terminator
  - .terraform
  - .vagrant
  - .yt_dlp
  - .zathura
