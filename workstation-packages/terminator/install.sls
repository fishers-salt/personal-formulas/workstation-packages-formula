# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}

workstation-packages-terminator-install-installed:
  pkg.installed:
    - name: {{ workstation_packages.terminator.pkg.name }}

{# Can't install terminator in test env - salt creates these files manually #}
{%- set testing_env = workstation_packages.get('testing_env', false) %}
{%- if testing_env %}
workstation-packages-terminator-install-clean-salt-artifacts:
  cmd.run:
    - name: "rm -rf
        /usr/lib/python3.10/site-packages/psutil
        /usr/lib/python3.10/site-packages/psutil-5.9.5.dist-info"
    - onfail:
      - pkg: workstation-packages-terminator-install-installed
    - onfail_stop: false
  pkg.installed:
    - name: {{ workstation_packages.terminator.pkg.name }}
{%- endif %}
