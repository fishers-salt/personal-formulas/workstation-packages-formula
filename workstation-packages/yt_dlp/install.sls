# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}

workstation-packages-yt-dlp-install-installed:
  pkg.installed:
    - name: {{ workstation_packages.yt_dlp.pkg.name }}

{# Can't install yt-dlp in test env - salt creates these files manually #}
{%- set testing_env = workstation_packages.get('testing_env', false) %}
{%- if testing_env %}
workstation-packages-yt-dlp-install-clean-salt-artifacts:
  cmd.run:
    - name: "rm -rf
        /usr/lib/python3.10/site-packages/certifi
        /usr/lib/python3.10/site-packages/certifi-2022.12.7.dist-info"
    - onfail:
      - pkg: workstation-packages-yt-dlp-install-installed
    - onfail_stop: false
  pkg.installed:
    - name: {{ workstation_packages.yt_dlp.pkg.name }}
{%- endif %}
