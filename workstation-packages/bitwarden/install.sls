# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}
{%- set sls_pipewire_install = tplroot ~ '.pipewire.install' %}

workstation-packages-bitwarden-install-installed:
  pkg.installed:
    - pkgs: {{ workstation_packages.bitwarden.pkgs }}
    - require:
      - sls: {{ sls_pipewire_install }}
