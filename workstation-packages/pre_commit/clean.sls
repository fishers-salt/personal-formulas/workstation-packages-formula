# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}

workstation-packages-pre-commit-clean-removed:
  pkg.removed:
    - name: {{ workstation_packages.pre_commit.pkg.name }}
