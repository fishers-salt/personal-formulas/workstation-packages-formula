# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}
{%- set sls_pipewire_install = tplroot ~ '.pipewire.install' %}

workstation-packages-gnome-install-deps-installed:
  pkg.installed:
    - pkgs: {{ workstation_packages.gnome_deps.pkgs }}
    - require:
      - sls: {{ sls_pipewire_install }}

workstation-packages-gnome-install-installed:
  pkg.installed:
    - pkgs: {{ workstation_packages.gnome.pkgs }}
    - require:
      - pkg: workstation-packages-gnome-install-deps-installed
      - sls: {{ sls_pipewire_install }}

{# Can't install gnome in test env - salt creates these files manually #}
{%- set testing_env = workstation_packages.get('testing_env', false) %}
{%- if testing_env %}
workstation-packages-gnome-install-clean-salt-artifacts:
  cmd.run:
    - name: "rm -rf
        /usr/lib/python3.10/site-packages/idna
        /usr/lib/python3.10/site-packages/idna-3.4.dist-info
        /usr/lib/python3.10/site-packages/jinja2
        /usr/lib/python3.10/site-packages/markupsafe
        /usr/lib/python3.10/site-packages/requests
        /usr/lib/python3.10/site-packages/requests-2.28.2.dist-info
        /usr/lib/python3.10/site-packages/urllib3"
    - onfail:
      - pkg: workstation-packages-gnome-install-installed
    - onfail_stop: false
    - require:
      - workstation-packages-gnome-install-deps-installed
  pkg.installed:
    - pkgs: {{ workstation_packages.gnome.pkgs }}
{%- endif %}
