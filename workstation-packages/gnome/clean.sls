# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}

workstation-packages-gnome-clean-removed:
  pkg.removed:
    - pkgs: {{ workstation_packages.gnome.pkgs }}

workstation-packages-gnome-clean-deps-removed:
  pkg.removed:
    - pkgs: {{ workstation_packages.gnome_deps.pkgs }}
    - require:
      - pkg: workstation-packages-gnome-clean-removed
