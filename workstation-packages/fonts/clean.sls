# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}

workstation-packages-fonts-clean-removed:
  pkg.removed:
    - pkgs: {{ workstation_packages.fonts.pkgs }}

workstation-packages-fonts-clean-custom-font-powerlevel-dir-removed:
  file.absent:
    - name: /usr/local/share/fonts/Powerlevel10k

workstation-packages-fonts-clean-powerlevel-update-font-cache:
  cmd.run:
    - name: fc-cache --force
    - watch:
      - workstation-packages-fonts-clean-custom-font-powerlevel-dir-removed
