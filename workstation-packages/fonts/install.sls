# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as workstation_packages with context %}

workstation-packages-fonts-install-installed:
  pkg.installed:
    - pkgs: {{ workstation_packages.fonts.pkgs }}

workstation-packages-fonts-install-custom-font-dir-managed:
  file.directory:
    - name: /usr/local/share/fonts
    - user: root
    - group: root
    - mode: '0755'
    - makedirs: True

workstation-packages-fonts-install-custom-font-powerlevel-dir-managed:
  file.directory:
    - name: /usr/local/share/fonts/Powerlevel10k
    - user: root
    - group: root
    - mode: '0755'
    - makedirs: True
    - require:
      - workstation-packages-fonts-install-custom-font-dir-managed

workstation-packages-fonts-install-powerlevel-regular-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Regular.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
    - source_hash: d97946186e97f8d7c0139e8983abf40a1d2d086924f2c5dbf1c29bd8f2c6e57d
    - require:
      - workstation-packages-fonts-install-custom-font-powerlevel-dir-managed

workstation-packages-fonts-install-powerlevel-bold-italic-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold Italic.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
    - source_hash: 56b4131adecec052c4b324efb818dd326d586dbc316fc68f98f1cae2eb8d1220
    - require:
      - workstation-packages-fonts-install-custom-font-powerlevel-dir-managed

workstation-packages-fonts-install-powerlevel-bold-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
    - source_hash: b6c0199cf7c7483c8343ea020658925e6de0aeb318b89908152fcb4d19226003
    - require:
      - workstation-packages-fonts-install-custom-font-powerlevel-dir-managed

workstation-packages-fonts-install-powerlevel-italic-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Italic.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
    - source_hash: 6f357bcbe2597704e157a915625928bca38364a89c22a4ac36e7a116dcd392ef
    - require:
      - workstation-packages-fonts-install-custom-font-powerlevel-dir-managed

workstation-packages-fonts-install-powerlevel-update-font-cache:
  cmd.run:
    - name: fc-cache --force
    - onchanges:
      - workstation-packages-fonts-install-powerlevel-regular-font-managed
      - workstation-packages-fonts-install-powerlevel-bold-italic-font-managed
      - workstation-packages-fonts-install-powerlevel-bold-font-managed
      - workstation-packages-fonts-install-powerlevel-italic-font-managed
